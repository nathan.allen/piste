# Gnar Piste

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![codecov](https://codecov.io/gl/gnaar/piste/branch/master/graph/badge.svg?token=MRowdXaujg)](https://codecov.io/gl/gnaar/piste)
[![pipeline status](https://gitlab.com/gnaar/piste/badges/master/pipeline.svg)](https://gitlab.com/gnaar/piste/commits/master)

**Part of Project Gnar:** &nbsp;[base](https://hub.docker.com/r/gnar/base) &nbsp;•&nbsp; [gear](https://pypi.org/project/gnar-gear) &nbsp;•&nbsp; [piste](https://gitlab.com/gnaar/piste) &nbsp;•&nbsp; [off-piste](https://gitlab.com/gnaar/off-piste) &nbsp;•&nbsp; [edge](https://www.npmjs.com/package/gnar-edge) &nbsp;•&nbsp; [powder](https://gitlab.com/gnaar/powder) &nbsp;•&nbsp; [genesis](https://gitlab.com/gnaar/genesis) &nbsp;•&nbsp; [patrol](https://gitlab.com/gnaar/patrol)

**Get started with Project Gnar on** &nbsp;[![Project Gnar on Medium](https://s3-us-west-2.amazonaws.com/project-gnar/medium-68x20.png)](https://medium.com/@ic3b3rg/project-gnar-d274165793b6)

**Join Project Gnar on** &nbsp;[![Project Gnar on Slack](https://s3-us-west-2.amazonaws.com/project-gnar/slack-69x20.png)](https://join.slack.com/t/project-gnar/shared_invite/enQtNDM1NzExNjY0NjkzLWQ3ZTQyYjgwMjkzNWYxNDJiNTQzODY0ODRiMmZiZjVkYzYyZWRkOWQzNjA0OTk3NWViNWM5YTZkMGJlOGIzOWE)

**Support Project Gnar on** &nbsp;[![Project Gnar on Patreon](https://s3-us-west-2.amazonaws.com/project-gnar/patreon-85x12.png)](https://patreon.com/project_gnar)

Project Gnar is a full stack, turnkey starter web app which includes:

- Sign up email with secure account activation
- Secure login and session management via JWT
- Basic account details (name, address, etc.)
- Password reset email with secure confirmation
- React-based frontend and Python-based microservice backend
- Microservice intra-communication
- SQS message polling and sending
- AWS Cloud-based hosting and Terraform + Kubernetes deployment

A demo site is up at [gnar.ca](https://app.gnar.ca) - you're welcome to create an account and test the workflows.

Gnar Piste is the main Python microservice of Project Gnar. It's built with [Gnar Gear](https://pypi.org/project/gnar-gear).

In development, start Piste with

```bash
$ python3 app/main.py -p 9400
```

The development server is a fault-tolerant version of [Flask's WSGI Development Server](http://flask.pocoo.org/docs/1.0/server), provided by [Gnar Gear](https://pypi.org/project/gnar-gear).

For production, build a [Gnar Base](https://hub.docker.com/r/gnar/base)-based Docker image using Piste's [Dockerfile](https://gitlab.com/gnaar/piste/blob/master/Dockerfile) and deploy it to a Kubernetes cluster with [Gnar Patrol](https://gitlab.com/gnaar/patrol)'s [piste-deployment.yml](https://gitlab.com/gnaar/patrol/blob/master/manifest/piste-deployment.yml).

```bash
$ docker build -t gnar/piste:latest .  # Replace 'gnar' with your Docker / ECR account name
```

The production server is a [Bjoern WSGI Production Server](https://github.com/jonashaag/bjoern), also provided by [Gnar Gear](https://pypi.org/project/gnar-gear).

On startup, Gnar Gear configures the following services:
- Logger & error handler
- Argon2 password hasher
- Postgres database connection
- JWT handler
- SQS poll
- Flask blueprints (auto-registered)

The configuration of these services is dependent on the following environment variables:

```bash
GNAR_EMAIL_HOST
GNAR_EMAIL_LOGO_URL
GNAR_EMAIL_PROJECT_NAME
GNAR_EMAIL_SENDER
GNAR_JWT_SECRET_KEY
GNAR_PG_DATABASE
GNAR_PG_ENDPOINT
GNAR_PG_PASSWORD
GNAR_PG_USERNAME
GNAR_RECAPTCHA_SECRET_KEY
GNAR_REDIS_HOST
GNAR_SES_ACCESS_KEY_ID
GNAR_SES_REGION_NAME
GNAR_SES_SECRET_ACCESS_KEY
GNAR_SQS_ACCESS_KEY_ID
GNAR_SQS_REGION_NAME
GNAR_SQS_SECRET_ACCESS_KEY
```

See the [Gnar Gear docs](https://pypi.org/project/gnar-gear) for details on these environment variables as well as other, optional, envars.

### Blueprints

- **/10-45** GET: Submits a `/check-in` request to [Off-Piste](https://gitlab.com/gnaar/off-piste) which sends an SQS message and responds with its message id
- **/10-45/get-message** GET: Picks up the message from the `/10-45` request (from Redis)
- **/user/activate** POST: Validates an activation id (from a `/user/signup` request) and returns a jwt access token
- **/user/get** POST: Jwt-protected route which retrieves a user's account details by email address
- **/user/login** POST: ReCAPTCHA-guarded route which authenticates a user by email address and password, returns the user's account details and a jwt token
- **/user/reset-password** POST: Validates an action id (from a `/user/send-reset-password-email` request) and updates the user's password hash
- **/user/score-password** POST: Returns the [zxcvbn](https://github.com/dropbox/zxcvbn) score of a password
- **/user/send-reset-password-email** POST: ReCAPTCHA-guarded route which sends a password reset email to a validated user
- **/user/signup** POST: ReCAPTCHA-guarded route which sends an activation email to the submitted email address
- **/user<user_id>** PUT: Jwt-protected route which updates a user's account details

---

<p><div align="center">Made with <img alt='Love' width='32' height='27' src='https://s3-us-west-2.amazonaws.com/project-gnar/heart-32x27.png'> by <a href='https://www.linkedin.com/in/briengivens'>Brien Givens</a></div></p>
