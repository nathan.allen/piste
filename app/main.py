from argparse import ArgumentParser
from logging import getLogger
from os import getenv

import redis
from gnar_gear import GnarApp

log = getLogger()

parser = ArgumentParser()
parser.add_argument('-p', '--port', type=int)
args = parser.parse_args()
port = vars(args)['port'] or 80
production = port == 80 and getenv('GNAR_ENV', '') != 'development'

# [Project Gnar]: If you're using `env_prefix` with GnarApp, you need to add it to `GNAR_REDIS_ENDPOINT` below.

redis_host = production and getenv('GNAR_REDIS_ENDPOINT')
redis_connection = redis.StrictRedis(host=getenv('GNAR_REDIS_ENDPOINT'), port=6379) if redis_host else None


def receive_sqs_message(message):
    redis_connection.set(message['MessageId'], message['Body'], 3600)


if __name__ == '__main__':
    sqs = {'queue_name': 'gnar-queue', 'callback': receive_sqs_message} if redis_host else None
    GnarApp('piste', production, port, sqs=sqs).run()
